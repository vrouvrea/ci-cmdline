"""Managing users"""

import getpass
import typing

import yaml

import api.crawler
import api.portal
import commandline
import connections
import tables

Name = typing.NewType("Name", str)
Name.__doc__ = "user name"

UsersListFilter = typing.NewType("UsersListFilter", str)
tables.init_list_filter_type(UsersListFilter, api.crawler.USER_LIST_SPEC)

FirstName = typing.NewType("FirstName", str)
FirstName.__doc__ = "first name"

LastName = typing.NewType("LastName", str)
LastName.__doc__ = "last name"

Email = typing.NewType("Email", str)
Email.__doc__ = "email"

Password = typing.NewType("Password", str)
Password.__doc__ = """\
    password

    If no PASSWORD is given, password is prompted from the terminal.
    """

Credentials = typing.NewType("Credentials", str)
Credentials.__doc__ = """\
    target filename for saving credentials

    If no CREDENTIALS filename is given, credentials are not saved.
    """

Pendings = typing.NewType("Pendings", bool)
Pendings.__doc__ = "pendings"


@commandline.RegisterCategory
class User(commandline.Category):
    """managing users"""

    # Front-end commands can have many parameters...
    # pylint: disable=too-many-arguments
    @commandline.RegisterAction
    def create(
        self,
        first_name: FirstName,
        last_name: LastName,
        email: Email,
        password: typing.Optional[Password] = None,
        credentials: typing.Optional[Credentials] = None,
    ) -> None:
        """create a new user"""
        if password is None:
            password = Password(getpass.getpass())
            confirm = getpass.getpass(prompt="Confirm: ")
            if password != confirm:
                raise commandline.Failure("Passwords mismatch.")
        with connections.Connections(self.config) as connection:
            connection.portal.users.create(
                first_name, last_name, email, password
            )
            if credentials is not None:
                store = {"username": email, "password": password}
                with open(credentials, "w") as file:
                    yaml.dump(store, file)

    @commandline.RegisterAction
    def suspend(self, email: Email) -> None:
        """suspend a user"""
        with connections.Connections(self.config) as connection:
            connection.portal.users[email].suspend()

    @commandline.RegisterAction
    def resume(self, email: Email) -> None:
        """resume a user"""
        with connections.Connections(self.config) as connection:
            connection.portal.users[email].resume()

    @commandline.RegisterAction
    def delete(self, email: Email) -> None:
        """delete a user"""
        with connections.Connections(self.config) as connection:
            connection.portal.users[email].delete()

    @commandline.RegisterAction
    def list(
        self,
        list_filter: typing.Iterable[UsersListFilter],
        pendings: Pendings = Pendings(False),
    ) -> None:
        """list users"""
        with connections.Connections(self.config) as connection:
            if pendings:
                list_to_show: typing.Sequence[
                    typing.Mapping[str, typing.Any]
                ] = api.crawler.remap_table(
                    connection.portal.get("users", "pendings"),
                    api.crawler.USER_LIST_SPEC,
                )
            else:
                list_to_show = tables.listable_to_table(connection.portal.users)
            tables.filter_and_show_list(
                list_to_show, list_filter, self.config.compact
            )

    @commandline.RegisterAction
    def uid(self, name: typing.Optional[Name] = None) -> None:
        """print user uid"""
        with connections.Connections(self.config) as connection:
            print(connection.portal.users[name].uid)
