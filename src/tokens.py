"""Managing Jenkins tokens"""

import typing

import jenkins

import commandline
import connections
import tables
import projects
import volumes

NewName = typing.NewType("NewName", str)
NewName.__doc__ = "token name"


def complete_token_name(**kwargs):
    """completer for token name"""
    parsed_args = kwargs["parsed_args"]
    with connections.NoninteractiveConnections(
        parsed_args.credentials, subdomain=parsed_args.project
    ) as connection:
        token_list = get_list(connection)
        return match_table(token_list).keys()


Name = typing.NewType("Name", str)
Name.__doc__ = """\
    token name

    TOKEN are regular expressions which can match either token names,
    creation dates or UUIDs.
    Multiple matches are accepted, but the command will fail if a regular
    expression has no match.
    """
commandline.set_completer(Name, complete_token_name)

LIST_SPEC = ["_uuid", "name", "creation_date", "use"]

TokenListFilter = typing.NewType("TokenListFilter", str)
tables.init_list_filter_type(TokenListFilter, LIST_SPEC)


@commandline.RegisterCategory
class Token(commandline.Category):
    """managing Jenkins tokens"""

    @commandline.RegisterAction
    def create(
        self, project: projects.Name, name: typing.Optional[NewName] = None
    ) -> None:
        """create new token"""
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            print(
                connection.jenkins.create_token(connection.username, name=name)
            )

    @commandline.RegisterAction
    def list(
        self,
        project: projects.Name,
        list_filter: typing.Iterable[TokenListFilter],
    ) -> None:
        """list tokens"""
        volumes.simple_list(self.config, project, list_filter, get_list)

    @commandline.RegisterAction
    def delete(
        self, project: projects.Name, token: typing.Iterable[Name]
    ) -> None:
        """revoke tokens"""
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            token_list = get_list(connection)
            tokens = [
                a_token
                for pattern in token
                for a_token in find(token_list, pattern)
            ]
            invalid = []
            for uuid in tokens:
                try:
                    connection.jenkins.revoke_token(connection.username, uuid)
                except commandline.Failure as error:
                    invalid.append(f"{uuid} ({error})")
            commandline.report_invalid(invalid)


def get_list(connection: connections.Connections):
    """enumerate all tokens"""
    try:
        return connection.jenkins.get_tokens(connection.username)
    except jenkins.NotFoundException:
        raise commandline.Failure(f"Project not found: {connection.subdomain}")


def match_table(token_list):
    """convert list of tokens into a dictionary indexed by name and by UUID."""
    return {
        key: token
        for token in token_list
        for key in (token["name"], token["creation_date"], token["_uuid"])
    }


def find(token_list, pattern):
    """find a token, either by name, by date or by UUID."""
    matches = list(tables.match_dict(match_table(token_list), pattern))
    if not matches:
        raise commandline.Failure(f"{pattern} does not match any token")
    return {token["_uuid"] for token in matches}
