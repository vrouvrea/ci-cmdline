"""Managing SSH keys"""
import collections
import pathlib
import os
import typing

import sshpubkeys

import api.connection
import api.portal
import commandline
import connections
import tables
import users

Name = typing.NewType("Name", str)
Name.__doc__ = """\
    SSH key (either the key comment, the host part of it, or an
    unambiguous prefix of the key hash)
    """

All = typing.NewType("All", bool)
All.__doc__ = "all SSH keys"

ListFilter = typing.NewType("ListFilter", str)
tables.init_list_filter_type(
    ListFilter, ["hash", "comment", "_key", "_delete_url"]
)

Key = typing.Dict[str, typing.Any]

KeyFile = typing.NewType("KeyFile", str)
KeyFile.__doc__ = "SSH key file name"


@commandline.RegisterCategory
class SSHKey(commandline.Category):
    """managing SSH keys"""

    @commandline.RegisterAction
    def list(
        self,
        list_filter: typing.Iterable[ListFilter],
        user: typing.Optional[users.Name] = None,
    ) -> None:
        """list SSH keys"""
        with connections.Connections(self.config) as connection:
            keys = connection.portal.users[user].get_ssh_keys()
        rows = [
            {
                "hash": ssh_key.hash_sha256(),
                "comment": ssh_key.comment,
                "_key": key.key,
            }
            for key in keys
            for ssh_key in (sshpubkeys.SSHKey(key.key),)
        ]
        tables.filter_and_show_list(rows, list_filter, self.config.compact)

    @commandline.RegisterAction
    def add(self, file: typing.List[KeyFile]) -> None:
        """add SSH keys (by default, ~/.ssh/id_rsa.pub)"""
        if not file:
            id_rsa_pub = os.path.join(os.environ["HOME"], ".ssh/id_rsa.pub")
            file = [KeyFile(id_rsa_pub)]
        keys = [pathlib.Path(filename).read_text() for filename in file]
        with connections.Connections(self.config) as connection:
            for key in keys:
                connection.portal.ssh_keys.add(key)

    # The argument has to be called 'all'
    # pylint: disable=redefined-builtin
    @commandline.RegisterAction
    def delete(self, key: typing.Iterable[Name], all: All = All(False)) -> None:
        """delete some or all SSH keys"""
        with connections.Connections(self.config) as connection:
            keys = find(connection, key, all)
            for sshkey in keys:
                sshkey.delete()

    # The argument has to be called 'all'
    # pylint: disable=redefined-builtin
    @commandline.RegisterAction
    def show(self, key: typing.Iterable[Name], all: All = All(False)) -> None:
        """show some or all SSH keys"""
        with connections.Connections(self.config) as connection:
            keys = find(connection, key, all)
            for sshkey in keys:
                print(sshkey.key)


def get_user_argument(
    connection: connections.Connections, user: typing.Optional[users.Name]
) -> users.Name:
    """return user indicated as argument of --user if any, or current user
    otherwise."""
    if user is None:
        username = connection.username
        if username is None:
            raise commandline.Failure("No username available")
        return users.Name(username)
    return user


def find(
    connection: connections.Connections,
    keys: typing.Iterable[Name],
    find_all: All = All(False),
    user: typing.Optional[users.Name] = None,
) -> typing.Iterable[api.portal.SSHKey]:
    """find a key"""
    user_keys = connection.portal.users[user].get_ssh_keys()
    if find_all:
        return user_keys
    key_infos = [(key, sshpubkeys.SSHKey(key.key)) for key in user_keys]
    comments = index_comments(key_infos)
    result_keys = []
    for key_name in keys:
        selected_keys = comments[key_name]
        key_count = len(selected_keys)
        if key_count == 1:
            selected_key = selected_keys[0]
        elif key_count > 1:
            raise commandline.Failure(f"Ambiguous key comment: {key_name}.")
        else:
            if key_name.startswith("SHA256:"):
                sha256_key_name: str = key_name
            else:
                sha256_key_name = "SHA256:" + key_name
            selected_key = None
            for key, infos in key_infos:
                if infos.hash_sha256().startswith(sha256_key_name):
                    if selected_key is not None:
                        raise commandline.Failure(
                            f"Ambiguous key prefix: {key_name}."
                        )
                    selected_key = key
            if selected_key is None:
                raise commandline.Failure(f"Unknown SSH key: {key_name}.")
        result_keys.append(selected_key)
    return result_keys


def index_comments(key_infos):
    """make a dictionary of user keys indexed key comments"""
    comments = collections.defaultdict(list)
    for key, infos in key_infos:
        comment = infos.comment
        comments[comment].append(key)
        try:
            _username, host = comment.split("@")
            comments[host].append(key)
        except ValueError:
            pass
    return comments
