"""Managing volumes"""

import os
import typing

import cs
import wget

import api.lazy
import api.spec
import commandline
import connections
import tables
import projects
import vms


def complete_volume(**kwargs):
    """complete volume argument"""
    parsed_args = kwargs["parsed_args"]
    with connections.NoninteractiveConnections(
        parsed_args.credentials, subdomain=parsed_args.project
    ) as connection:
        return [volume["name"] for volume in get_list(connection)]


Name = typing.NewType("Name", str)
Name.__doc__ = """\
    volume name

    VOLUME can be either a volume id, or a volume name,
    or the name of a virtual machine to refer to the ROOT disk,
    or vm_name:DATADISK to refer to the DATADISK.
    """
commandline.set_completer(Name, complete_volume)

LIST_SPEC = [
    "id",
    "name",
    "vmname",
    "vmstate",
    "size",
    "type",
    "_zoneid",
    "_virtualmachineid",
]

ListFilter = typing.NewType("ListFilter", str)
tables.init_list_filter_type(ListFilter, LIST_SPEC)

Detach = typing.NewType("Detach", bool)
Detach.__doc__ = "detach module first if attached"


@commandline.RegisterCategory
class Volume(commandline.Category):
    """managing volumes"""

    Size = typing.NewType("Size", int)
    Size.__doc__ = "volume size (in GB)"

    # Front-end commands can have many parameters...
    # pylint: disable=too-many-arguments
    @commandline.RegisterAction
    def create(
        self,
        project: projects.Name,
        size: Size,
        name: typing.Optional[Name] = None,
        ci_admin: vms.CiAdmin = vms.CiAdmin(False),
        attach_to: typing.Optional[vms.Name] = None,
    ) -> None:
        """create a new storage volume"""
        if ci_admin:
            subdomain = None
        else:
            subdomain = project
        with connections.Connections(
            self.config, subdomain=subdomain
        ) as connection:
            args = {}
            if ci_admin:
                args["account"] = "admins"
                args["domainid"] = connection.get_project_domainid(project)
            # diskOfferingId=28: custom
            volume = connection.cloudstack.createVolume(
                zoneId=connection.zone_id,
                name=name,
                diskOfferingId=28,
                size=size,
                **args,
            )
            volume_id = volume["volume"]["id"]
            if not self.config.quiet:
                commandline.output_message(f"Volume created: {volume_id}.")
            if attach_to is not None:
                vm_dict = tables.dict_of_list(
                    vms.get_list_from_cloudstack(connection, **args), "id"
                )
                vm_id, _selected_vm = vms.find(
                    vm_dict, project, attach_to, required=True
                )
                connection.cloudstack.attachVolume(
                    id=volume_id, virtualMachineId=vm_id, **args
                )

    NewSize = typing.NewType("NewSize", str)
    NewSize.__doc__ = """\
        volume new size (in GB)

        NEW-SIZE can be relative to the current size of the volume,
        e.g. +10, *2...
        """

    @commandline.RegisterAction
    def resize(
        self,
        project: projects.Name,
        volume: Name,
        new_size: NewSize,
        ci_admin: vms.CiAdmin = vms.CiAdmin(False),
    ) -> None:
        """resize storage volume"""
        relative = commandline.str_to_relative(new_size)
        with connections.Connections(
            self.config, subdomain=project, ci_admin=ci_admin
        ) as connection:
            args = connection.cloudstack_args()
            volume_id, selected_volume = find(
                connection, project, volume, **args
            )
            size = relative(
                lambda: selected_volume["size"] // 1024 // 1024 // 1024
            )
            if size < 0:
                raise commandline.Failure("Size cannot be negative")
            # diskOfferingId=28: custom
            connection.cloudstack.resizeVolume(
                id=volume_id, diskOfferingId=28, size=size
            )

    @commandline.RegisterAction
    def attach(
        self,
        project: projects.Name,
        volume: Name,
        machine: vms.Name,
        ci_admin: vms.CiAdmin = vms.CiAdmin(False),
    ) -> None:
        """attach a volume to a virtual machine"""
        with connections.Connections(
            self.config, subdomain=project, ci_admin=ci_admin
        ) as connection:
            args = connection.cloudstack_args()
            find_volume = Find(connection, **args)
            volume_id, _selected_volume = find_volume.find(project, volume)
            vm_id, _selected_vm = vms.find(
                find_volume.vm_dict, project, machine, required=True
            )
            connection.cloudstack.attachVolume(
                id=volume_id, virtualMachineId=vm_id, **args
            )

    @commandline.RegisterAction
    def detach(
        self,
        project: projects.Name,
        volume: Name,
        ci_admin: vms.CiAdmin = vms.CiAdmin(False),
    ) -> None:
        """detach a volume from a virtual machine"""
        with connections.Connections(
            self.config, subdomain=project, ci_admin=ci_admin
        ) as connection:
            args = connection.cloudstack_args()
            volume_id, _selected_volume = find(
                connection, project, volume, **args
            )
            connection.cloudstack.detachVolume(id=volume_id, **args)

    @commandline.RegisterAction
    def download(
        self,
        project: projects.Name,
        volume: Name,
        output: typing.Optional[commandline.OutputFilename] = None,
        ci_admin: vms.CiAdmin = vms.CiAdmin(False),
    ) -> None:
        """\
        download a detached volume

        If no FILENAME is given, the volume name will be used instead.
        """
        with connections.Connections(
            self.config, subdomain=project, ci_admin=ci_admin
        ) as connection:
            args = connection.cloudstack_args()
            volume_id, selected_volume = find(
                connection, project, volume, **args
            )
            response = connection.cloudstack.extractVolume(
                id=volume_id,
                zoneid=selected_volume["_zoneid"],
                mode="HTTP_DOWNLOAD",
                **args,
            )
            url = response["volume"]["url"]
            if output is None:
                output = selected_volume["name"] + os.path.splitext(url)[1]
            wget.download(url, output)

    @commandline.RegisterAction
    def delete(
        self,
        project: projects.Name,
        volume: Name,
        ci_admin: vms.CiAdmin = vms.CiAdmin(False),
        detach: Detach = Detach(False),
    ) -> None:
        """delete a volume"""
        with connections.Connections(
            self.config, subdomain=project, ci_admin=ci_admin
        ) as connection:
            args = connection.cloudstack_args()
            volume_id, _selected_volume = find(
                connection, project, volume, **args
            )
            if detach:
                try:
                    connection.cloudstack.detachVolume(id=volume_id, **args)
                except cs.client.CloudStackApiException:
                    pass
            connection.cloudstack.deleteVolume(id=volume_id, **args)

    @commandline.RegisterAction
    def list(
        self,
        project: projects.Name,
        list_filter: typing.Iterable[ListFilter],
        ci_admin: vms.CiAdmin = vms.CiAdmin(False),
    ) -> None:
        """list storage volumes"""
        with connections.Connections(
            self.config, subdomain=project, ci_admin=ci_admin
        ) as connection:
            rows = get_list(connection, **connection.cloudstack_args())
        tables.filter_and_show_list(rows, list_filter, self.config.compact)


def simple_list(config, project, list_filter, list_function):
    """simple implementation of list command"""
    with connections.Connections(config, subdomain=project) as connection:
        rows = list_function(connection)
    tables.filter_and_show_list(rows, list_filter, config.compact)


class Find:
    """find volume"""

    def __init__(self, connection: connections.Connections, **args):
        self.__connection = connection
        self.__args = args
        self.__volume_dict = api.lazy.lazy(self.__init_volume_dict)
        self.__vm_dict = api.lazy.lazy(self.__init_vm_dict)

    @property
    def volume_dict(self):
        """return volume dict"""
        return self.__volume_dict()

    def __init_volume_dict(self):
        return tables.dict_of_list(
            get_list(self.__connection, **self.__args), "id"
        )

    @property
    def vm_dict(self):
        """return VM dict"""
        return self.__vm_dict()

    def __init_vm_dict(self):
        return tables.dict_of_list(
            vms.get_list_from_cloudstack(self.__connection, **self.__args), "id"
        )

    def find(self, project: projects.Name, volume: Name):
        """find volume"""
        volume_dict = self.volume_dict
        try:
            return volume, volume_dict[volume]
        except KeyError:
            for volume_id, desc in volume_dict.items():
                if desc["name"] == volume:
                    return volume_id, desc
            try:
                vm_name, volume_type = volume.split(":")
            except ValueError:
                vm_name = volume
                volume_type = "ROOT"
            try:
                _vm_id, machine = vms.find(self.vm_dict, project, vm_name)
                vm_hostname = machine["hostname"]
                for volume_id, desc in volume_dict.items():
                    name = desc["vmname"]
                    if name == vm_hostname and desc["type"] == volume_type:
                        return volume_id, desc
            except (TypeError, ValueError):
                pass
            raise commandline.Failure(f"Unknown volume: {volume}.")


def find(connection, project, volume, **args):
    """find volume"""
    return Find(connection, **args).find(project, volume)


def get_list(connection, **args):
    """enumerate volumes"""
    volumes = connection.cloudstack.listVolumes(fetch_list=True, **args)
    return api.spec.remap_dict_list(volumes, LIST_SPEC)
