"""Managing Jenkins job queue"""

import typing

import api.crawler
import commandline
import connections
import tables
import projects

LIST_SPEC: api.spec.DictSpec = {
    "id": "id",
    "blocked": ("blocked", api.crawler.yes_no_of_bool),
    "buildable": ("buildable", api.crawler.yes_no_of_bool),
    "stuck": ("stuck", api.crawler.yes_no_of_bool),
    "why": "why",
    "pending": ("pending", api.crawler.yes_no_of_bool),
}

ListFilter = typing.NewType("ListFilter", str)
tables.init_list_filter_type(ListFilter, LIST_SPEC)

Filter = typing.NewType("Filter", str)
tables.init_filter_type(Filter, LIST_SPEC)


@commandline.RegisterCategory
class Queue(commandline.Category):
    """managing Jenkins job queue"""

    @commandline.RegisterAction
    def list(
        self, project: projects.Name, list_filter: typing.Iterable[ListFilter]
    ) -> None:
        """list Jenkins job queue"""
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            response = connection.retry_loop(connection.jenkins.get_job_queue)
            rows = api.spec.remap_dict_list(response, LIST_SPEC)
        tables.filter_and_show_list(rows, list_filter, self.config.compact)

    @commandline.RegisterAction
    def cancel(
        self, project: projects.Name, cancel_filter: typing.Iterable[Filter]
    ) -> None:
        """cancel Jenkins jobs from queue"""
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            response = connection.retry_loop(connection.jenkins.get_job_queue)
            rows = api.spec.remap_dict_list(response, LIST_SPEC)
        for row in tables.filter_list(rows, cancel_filter):
            connection.jenkins.cancel_queued_job(int(row["id"]))
