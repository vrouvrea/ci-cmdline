"""Tools for crawling portal"""

import typing
import typing_extensions

import bs4

import api.connection
import api.spec


TableDict = typing.Sequence[typing.Mapping[str, bs4.element.Tag]]


def list_of_table(table: typing.Optional[bs4.element.Tag]) -> TableDict:
    """convert an HTML table to a list of dictionaries."""
    if not table:
        return []
    columns = [
        header.text.strip()
        for thead in table.find_all("thead")
        if isinstance(thead, bs4.element.Tag)
        for header in thead.find_all("th")
    ]
    rows = [
        tr
        for tbody in table.find_all("tbody")
        if isinstance(tbody, bs4.element.Tag)
        for tr in tbody.find_all("tr")
    ]
    result = []
    for row in rows:
        cells = row.find_all("td")
        entry = dict(zip(columns, cells))
        entry["_row"] = row
        result.append(entry)
    return result


def get_single_attribute(element: bs4.element.Tag, attr: str) -> str:
    result = element[attr]
    if not isinstance(result, str):
        raise Exception(f"Excepted single attribute {attr}: {element}")
    return result


def get_child_tag(
    element: bs4.element.Tag, name: str, **kwargs
) -> bs4.element.Tag:
    result = element.find(name, **kwargs)
    if not isinstance(result, bs4.element.Tag):
        raise Exception(f"Excepted child tag {name}: {element}")
    return result


def get_element_id(element: bs4.element.Tag) -> str:
    """element interpreter: return cell id"""
    return get_single_attribute(element, "id")


def get_cell_text(cell: bs4.element.Tag) -> str:
    """cell interpreter: return cell text"""
    if not cell:
        return ""
    return cell.text.replace("\n", " ").strip()


def decode_email(email: str) -> str:
    """unscramble email address"""
    return email.replace(" (at) ", "@").replace(" (dot) ", ".")


def get_cell_email(cell: bs4.element.Tag) -> str:
    """cell interpreter: return cell text, unscrambling email address"""
    return decode_email(get_cell_text(cell))


def get_input_name(cell: bs4.element.Tag) -> str:
    """cell interpreter: return input field name"""
    return decode_email(
        get_single_attribute(get_child_tag(cell, "input"), "name")
    )


def get_left_component(cell: bs4.element.Tag) -> str:
    """extract first component from A/B cell"""
    return get_cell_text(cell).split(" / ")[0]


def get_right_component(cell: bs4.element.Tag) -> str:
    """extract right component from A/B cell"""
    return get_cell_text(cell).split(" / ")[1]


def get_expire(cell: bs4.element.Tag) -> str:
    """extract date from expire cell"""
    return cell.get_text().strip()


def yes_no_of_bool(
    boolean: bool,
) -> typing.Union[
    typing_extensions.Literal["yes"], typing_extensions.Literal["no"]
]:
    """convert a Boolean to yes/no string"""
    if boolean:
        return "yes"
    return "no"


def is_checked(input_elt) -> bool:
    """return whether the input element input_elt is checked"""
    return "checked" in input_elt.attrs


def get_input_check(input_value: str) -> api.spec.AnyInterpreter:
    """cell interpreter: return yes/no whether a given check-box is checked"""

    def interpreter(cell: bs4.element.Tag) -> str:
        return yes_no_of_bool(is_checked(cell.find("input", value=input_value)))

    return interpreter


def remap_table_dict(
    items: TableDict, spec: api.spec.DictSpec
) -> typing.List[typing.Mapping[str, typing.Any]]:
    """Change list of dictionaries (from list_of_table) according to a table
    specification"""
    return api.spec.remap_dict_list(
        items, spec, default_interpreter=get_cell_text
    )


def remap_table(
    table: typing.Optional[bs4.element.Tag], spec: api.spec.DictSpec
) -> typing.List[typing.Mapping[str, typing.Any]]:
    """Convert an HTML table according to a table
    specification"""
    return remap_table_dict(list_of_table(table), spec)


def check_server_response(response) -> None:
    """check that response is a success or raise a failure"""
    if response.status_code == 200:
        return
    raise_server_error(response)


def check_server_error(page) -> typing.Optional[str]:
    """return text in error alert box if any"""
    # Class 'error' is not used systematically
    # (for instance, '/users/active' has only 'alert alert-error').
    error_div = page.body.find("div", class_="alert-error")
    if error_div is None:
        return None
    return get_alert_text(error_div)


def parse_server_error(response) -> typing.Optional[str]:
    """parse response as HTML and return text in error alert box if any"""
    page = bs4.BeautifulSoup(response.content, features="lxml")
    return check_server_error(page)


def raise_server_error(response) -> None:
    """raise a failure, extracting the error text from the server response if
    any"""
    error = parse_server_error(response)
    if error is None:
        raise api.connection.QueryError("Invalid server response.")
    raise api.connection.QueryError(error)


def get_alert_text(alert_div: bs4.element.Tag) -> str:
    """return the text inside an alert div element"""
    close_button = alert_div.find("button", class_="close")
    if isinstance(close_button, bs4.element.Tag):
        close_button.clear()
    link = alert_div.find("a")
    if isinstance(link, bs4.element.Tag):
        link.clear()
    return alert_div.text.strip()


USER_LIST_SPEC: api.spec.DictSpec = {
    "uid": "#",
    "first_name": "First name",
    "last_name": "Last name",
    "email": ("E-mail", get_cell_email),
    "type": "Type",
    "cri": "Cri",
}


VM_NAME_SPEC: api.spec.TextSpec = {
    """can contain ASCII letters 'a' through 'z', the digits '0'
    through '9', and the hyphen ('-')""": lambda s: all(
        "a" <= c <= "z" or "0" <= c <= "9" or c == "-" for c in s
    ),
    """must be between 1 and 63 characters long""": lambda s: 1 <= len(s) <= 63,
    '''can't start or end with "-"''': lambda s: s[0] != "-" and s[-1] != "-",
    """can't start with digit""": lambda s: not ("0" <= s[0] <= "9"),
}


VM_LIST_SPEC_MEMBER = {
    "id": ("_row", get_element_id),
    "status": "Status",
    "display_name": "Display name",
    "hostname": ("Hostname/IP", get_left_component),
    "ip": ("Hostname/IP", get_right_component),
    "os": "OS",
    "cpu": ("CPU/Memory", get_left_component),
    "memory": ("CPU/Memory", get_right_component),
    "created": "Created",
    "expire": ("Expire", get_expire),
}

VM_LIST_SPEC_NOT_MEMBER: api.spec.DictSpec = {
    key: value
    for key, value in VM_LIST_SPEC_MEMBER.items()
    if key not in ("hostname", "ip")
}


def get_admin_id(cell: bs4.element.Tag) -> str:
    """return admin id from table cell"""
    return get_single_attribute(
        get_child_tag(cell, "input", value="admin"), "id"
    ).split("~")[0]


MEMBER_LIST_SPEC_MEMBER = {
    "uid": "#",
    "first_name": "First name",
    "last_name": "Last name",
    "email": ("E-mail", get_cell_email),
    "cri": "Cri",
    "admin": ("Roles", get_input_check("admin")),
    "slave_admin": ("Roles", get_input_check("slaveAdmin")),
    "_admin_id": ("Roles", get_admin_id),
}

MEMBER_LIST_SPEC_NOT_MEMBER: api.spec.DictSpec = {
    key: value
    for key, value in MEMBER_LIST_SPEC_MEMBER.items()
    if key not in ("#", "_admin_id")
}

PENDING_USER_LIST_SPEC: api.spec.DictSpec = {
    "user": "User",
    # Note that e-mails are not scrambled in this table!
    "email": "Email",
    "_handle": ("Accept", get_input_name),
}

PROJECT_LOG_LIST_SPEC: api.spec.DictSpec = {
    "id": "ID",
    "date": "Date",
    "tags": "Tags",
    "message": "Message",
    "user": "User",
}


def get_short_name(element: bs4.element.Tag) -> str:
    """extract the short name of a project from the Join action link in the
    project list displayed for regular users"""
    link = element.find("a")
    if not isinstance(link, bs4.element.Tag):
        return ""
    return get_single_attribute(link, "href").replace("/project/join/", "")


PROJECT_LIST_SPEC_REGULAR_USER: api.spec.DictSpec = {
    "short_name": ("Actions", get_short_name),
    "full_name": "Full name",
    "description": "Description",
    "status": "Status",
}

PROJECT_LIST_SPEC_ADMIN: api.spec.DictSpec = {
    "#": "#",
    "short_name": "Short name",
    "full_name": "Full name",
    "status": "Status",
    "server": "Server",
    "port": "Port",
    "privacy": "Privacy",
    "software": "Software",
    "prod_version": "Prod. version",
    "qualif_version": "Qualif. version",
}

DELETED_PROJECT_LIST_SPEC: api.spec.DictSpec = {
    "short_name": "Short name",
    "full_name": "Full name",
    "admins": "Admin(s)",
    "deletion_timestamp": "Deleted by user on",
}

FormDict = typing.Dict[str, str]


def dict_of_form(form: bs4.element.Tag) -> FormDict:
    """convert HTML form into dictionary"""
    value_dict = {}
    for input_tag in form.find_all("input"):
        input_type = input_tag["type"]
        if (
            input_type in ["hidden", "text", "number"]
            or input_type == "radio"
            and input_tag.get("checked", "") == "checked"
        ):
            value_dict[input_tag["name"]] = input_tag["value"]
    for textarea_tag in form.find_all("textarea"):
        value_dict[textarea_tag["name"]] = textarea_tag.text
    for select_tag in form.find_all("select"):
        option_tag = select_tag.find("option", {"selected": True})
        value_dict[select_tag["name"]] = option_tag["value"]
    return value_dict


def fill_form(form: bs4.element.Tag, value_dict: FormDict) -> None:
    """fill HTML form"""
    for input_tag in form.find_all("input"):
        input_type = input_tag["type"]
        if input_type == "hidden":
            value_dict[input_tag["name"]] = input_tag["value"]
        elif input_type in ["text", "number"]:
            if input_tag.has_attr("required"):
                name = input_tag["name"]
                if not name in value_dict:
                    raise ValueError(f"Missing field {name}")


def element_value_or_none(
    element: typing.Optional[bs4.element.Tag],
) -> typing.Optional[str]:
    """return element value if not None"""
    if element is None:
        return None
    return get_single_attribute(element, "value")
