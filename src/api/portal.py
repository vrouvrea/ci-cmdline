# pylint: disable=too-many-lines
"""API to ci.inria.fr portal"""

# PEP 563 -- Postponed Evaluation of Annotations
# https://www.python.org/dev/peps/pep-0563/
# For instance, Portal.users return Users
from __future__ import annotations

import base64
import dataclasses
import enum
import logging
import types
import typing

import bs4
import requests

import api.crawler
import api.connection
import api.key_indexed
import api.lazy
import api.spec

LOGGER = logging.getLogger(__name__)

DEFAULT_MEMORY_LIMIT = 2


# Portal class has 8 instance attributes: refactor?
# pylint: disable=too-many-instance-attributes
class Portal:
    """connection management for ci.inria.fr"""

    def __init__(
        self,
        url: str,
        session: requests.Session,
        credentials_provider: api.connection.CredentialsProvider,
        timeout: int,
    ):
        self.__url = url
        self.__session = session
        self.__projects = Projects(self)
        self.__user: typing.Optional[User] = None
        self.__users = Users(self)
        self.__ssh_keys = SSHKeys(self)
        self.__credentials_provider = credentials_provider
        self.__timeout = timeout

    @property
    def timeout(self) -> int:
        """return connection timeout"""
        return self.__timeout

    @property
    def projects(self) -> Projects:
        """return projects API"""
        return self.__projects

    @property
    def user(self) -> User:
        """return connected user"""
        user = self.__user
        if user is None:
            user = self.login()
        return user

    @property
    def users(self) -> Users:
        """return users API"""
        return self.__users

    @property
    def ssh_keys(self) -> SSHKeys:
        """return ssh-keys API"""
        return self.__ssh_keys

    @property
    def session(self) -> requests.Session:
        """return session"""
        return self.__session

    def url(self, *path) -> str:
        """build URL for CI portal"""
        return api.connection.build_url(self.__url, *path)

    def request(
        self, path, get=False, login_if_needed=True, data=None, **kwargs
    ) -> requests.Response:
        """request a page (either with GET or POST)"""
        if login_if_needed and "PHPSESSID" not in self.session.cookies:
            self.login()
        url = self.url(*path)
        kwargs["timeout"] = self.timeout
        if get:
            try:
                del kwargs["headers"]
            except KeyError:
                pass
            query = lambda: self.session.get(url, params=data, **kwargs)
        else:
            query = lambda: self.session.post(url, data=data, **kwargs)
        response = query()
        if login_if_needed and (
            response.url == self.url("login")
            or response.status_code in (401, 403, 500)
        ):
            self.login()
            response = query()
        return response

    def query(
        self, path, get=False, login_if_needed=True, **kwargs
    ) -> bs4.BeautifulSoup:
        """request a page (either with GET or POST) and parse and check the
        result"""
        response = self.request(path, get, login_if_needed, **kwargs)
        page = bs4.BeautifulSoup(response.content, features="lxml")
        error_message = api.crawler.check_server_error(page)
        if error_message is not None:
            raise api.connection.QueryError(error_message)
        return page

    def get(self, *args, login_if_needed=True, **params) -> bs4.BeautifulSoup:
        """get a page from portal"""
        return self.query(args, True, login_if_needed, data=params)

    def command(
        self,
        *args,
        need_success_message=True,
        headers=None,
        get=False,
        login_if_needed=False,
        **data,
    ) -> bs4.BeautifulSoup:
        """send a command and check the success"""
        page = self.query(
            args, get, login_if_needed, data=data, headers=headers
        )
        if page.body is None:
            raise Exception("Expected body")
        success = page.body.find("div", class_="alert-success")
        if not success:
            success = page.body.find("div", id="flash_message")
            if not success and need_success_message:
                raise api.connection.QueryError("Failed without error message.")
        if isinstance(success, bs4.element.Tag):
            LOGGER.info(api.crawler.get_alert_text(success))
        return page

    def api_command(self, *args, get=False, login_if_needed=False, **data):
        """send a command through API (answer in JSON instead of HTML)"""
        response = self.request(
            ["api"] + list(args), get, login_if_needed, data=data
        )
        api.crawler.check_server_response(response)
        json = response.json()
        if json["responseCode"] != 200:
            raise api.connection.QueryError(json["message"])
        try:
            message = json["message"]
            LOGGER.info(message.replace("<br/>", " ").replace("<br>", " "))
        except KeyError:
            pass
        return json

    def login(self) -> User:
        """login to CI portal"""
        self.session.cookies.clear()
        login_page = self.get("login", login_if_needed=False)
        credentials_provider = self.__credentials_provider
        credentials = credentials_provider.get_credentials()
        username = credentials.username
        data = {"_username": username, "_password": credentials.password}
        if login_page.body is None:
            raise Exception("Expected body")
        api.crawler.fill_form(login_page.body, data)
        response = self.session.post(
            self.url("login_check"), timeout=self.timeout, data=data
        )
        if response.status_code != 200 or response.url == self.url("login"):
            api.crawler.raise_server_error(response)
        user = User(self, username)
        self.__user = user
        credentials_provider.store_cookies(self.session.cookies)
        return user

    class Section:
        """base class for portal section"""

        def __init__(self, portal: Portal):
            self.__portal = portal

        def command(self, *args, **data) -> bs4.BeautifulSoup:
            """send a command and check the success"""
            return self.portal.command(*args, **data)

        @property
        def portal(self):
            """return the associated connection to CI portal"""
            return self.__portal


class User(Portal.Section, api.spec.Listable, api.key_indexed.KeyIndexed):
    """user"""

    def __init__(
        self,
        portal: Portal,
        email: str,
        uid: typing.Optional[str] = None,
        as_map=None,
    ):
        Portal.Section.__init__(self, portal)
        api.spec.Listable.__init__(self, as_map)
        self.__email = email
        self.__uid = uid

    def _key(self):
        return self.__email

    def get_page(self) -> bs4.BeautifulSoup:
        """return the user profile page"""
        return self.portal.get("users", "show", self.__email)

    def get_page_body(self) -> bs4.element.Tag:
        result = self.get_page().body
        if result is None:
            raise Exception("Expected body")
        return result

    def get_projects(self) -> typing.Iterable[Project]:
        """enumerate all projects, user is member of"""
        page = self.get_page_body()
        project_list = page.find("ul", attrs={"class": "unstyled"})
        if not isinstance(project_list, bs4.element.Tag):
            return []
        return [
            self.portal.projects[row.find("a").text.strip()]
            for row in project_list.find_all("li")
        ]

    def __ssh_key_of_row(self, row) -> SSHKey:
        cells = row.find_all("td")
        return SSHKey(
            self.portal, str(cells[0].text.strip()), cells[1].find("a")["href"]
        )

    def get_ssh_keys(self) -> typing.Iterable[SSHKey]:
        """enumerate user ssh-keys"""
        page = self.get_page_body()
        tbody = api.crawler.get_child_tag(page, "tbody")
        return map(self.__ssh_key_of_row, tbody.find_all("tr")) if tbody else []

    @property
    def email(self) -> str:
        """return user email"""
        return self.__email

    @property
    def uid(self) -> str:
        """return user uid"""
        uid = self.__uid
        if uid is None:
            uid = api.crawler.get_child_tag(self.get_page_body(), "small").text
            self.__uid = uid
        return uid

    def suspend(self) -> None:
        """suspend user account"""
        self.portal.get("users", "suspend", self.email)

    def resume(self) -> None:
        """resume user account"""
        self.portal.get("users", "resume", self.email)

    def delete(self) -> None:
        """delete user account"""
        self.portal.get("apiusers", "delete", self.email)


class Users(Portal.Section):
    """user management"""

    def create(
        self, first_name: str, last_name: str, email: str, password: str
    ) -> User:
        """create a new user"""
        page = self.portal.get("users", "new", login_if_needed=False)
        data = {
            "new_user[fname]": first_name,
            "new_user[lname]": last_name,
            "new_user[email]": email,
            "new_user[password][first]": password,
            "new_user[password][second]": password,
        }
        if page.body is None:
            raise Exception("Expected body")
        api.crawler.fill_form(page.body, data)
        self.command("users", "new", **data, login_if_needed=False)
        return User(self.portal, email)

    def __user_of_dict(self, user) -> User:
        return User(self.portal, user["email"], uid=user["uid"], as_map=user)

    def __iter__(self) -> typing.Iterator[User]:
        """enumerate all users"""
        user_dict_list = api.crawler.remap_table(
            self.portal.get("users", "active"), api.crawler.USER_LIST_SPEC
        )
        users = map(self.__user_of_dict, user_dict_list)
        return iter(users)

    def __getitem__(self, key: typing.Optional[str]) -> User:
        """return the user associated to the given email, or the logged user if
        key is None."""
        if key is None:
            return self.portal.user
        return User(self.portal, key)


class SSHKey(Portal.Section, api.key_indexed.KeyIndexed):
    """SSH key"""

    def __init__(self, portal: Portal, key: str, delete_url: str):
        Portal.Section.__init__(self, portal)
        self.__key = key
        self.__delete_url = delete_url

    @property
    def key(self) -> str:
        """return the public key"""
        return self.__key

    def _key(self) -> str:
        return self.__key

    def delete(self) -> None:
        """delete the key"""
        self.command(self.__delete_url)


class SSHKeys(Portal.Section):
    """ssh-keys management"""

    def __init__(self, portal: Portal):
        super().__init__(portal)

        def get_page():
            return self.portal.user.get_page()

        self.__form = api.lazy.lazy(get_page)

    def __iter__(self) -> typing.Iterator[SSHKey]:
        return iter(self.portal.user.get_ssh_keys())

    def add(self, *keys: str) -> None:
        """add ssh-keys

        The function does not return the created key as an instance of SSHKey
        because we do not know how to compute the deletion URL."""
        for key in keys:
            data = {"add_ssh_key[ssh_key]": key}
            api.crawler.fill_form(self.__form(), data)
            self.command("users", "key", "add", **data)


class DeletableProject(Portal.Section, api.key_indexed.KeyIndexed):
    """deletable project"""

    def __init__(self, portal: Portal, short_name: str) -> None:
        super().__init__(portal)
        self.__short_name = short_name

    @property
    def short_name(self) -> str:
        """return project short name"""
        return self.__short_name

    def _key(self) -> str:
        return self.__short_name

    def really_delete(self) -> None:
        """really delete a project"""
        self.command("really-delete")

    def command(self, *args, **kwargs) -> bs4.BeautifulSoup:
        """send a command to the project"""
        return self.portal.command("project", self.short_name, *args, **kwargs)


def encode_base64(string: str) -> str:
    """encode a string into base64"""
    return str(base64.b64encode(string.encode("utf-8")), "utf-8")


class Project(DeletableProject, api.spec.Listable):
    """project"""

    class Section(Portal.Section):
        """base class for project section"""

        def __init__(self, project: Project):
            Portal.Section.__init__(self, project.portal)
            self.__project = project

        @property
        def project(self):
            """return the associated project"""
            return self.__project

        @property
        def portal(self):
            """return the associated portal"""
            return self.__project.portal

    @dataclasses.dataclass
    class Description:
        """project description"""

        full_name: typing.Optional[str] = None
        description: typing.Optional[str] = None
        public: typing.Optional[bool] = None

    def __init__(
        self,
        portal: Portal,
        short_name: str,
        description: Description,
        as_map=None,
    ):
        DeletableProject.__init__(self, portal, short_name)
        api.spec.Listable.__init__(self, as_map)
        self.__description = description
        self.__sub_apis = SubAPIs(
            vms=Vms(self), members=Members(self), jenkins=Jenkins(self)
        )

    @property
    def full_name(self) -> str:
        """return project full name"""
        if self.__description.full_name is not None:
            return self.__description.full_name
        return self.fetch_config().full_name

    @property
    def description(self) -> str:
        """return project description"""
        if self.__description.description is not None:
            return self.__description.description
        return self.fetch_config().description

    @property
    def public(self) -> bool:
        """return whether project is public"""
        if self.__description.public is not None:
            return self.__description.public
        return self.fetch_config().public

    @property
    def vms(self) -> Vms:
        """return vms API"""
        return self.__sub_apis.vms

    @property
    def members(self) -> Members:
        """return members API"""
        return self.__sub_apis.members

    @property
    def jenkins(self) -> Jenkins:
        """return Jenkins instances API"""
        return self.__sub_apis.jenkins

    def get(self, *args, **params) -> bs4.BeautifulSoup:
        """get project page"""
        return self.portal.get("project", self.short_name, *args, **params)

    def get_body(self, *args, **params) -> bs4.element.Tag:
        """get project page body"""
        result = self.get(*args, **params).body
        if result is None:
            raise Exception("Expected body")
        return result

    def api_command(self, *args, **data):
        """send API command to project"""
        return self.portal.api_command(
            "project", self.short_name, *args, **data
        )

    @dataclasses.dataclass
    class Config:
        """project configuration"""

        __project: Project
        __description: Project.Description
        full_name: str
        public: bool
        description: str
        jenkins: bool
        memory_limit: int
        server: typing.Optional[str]
        token: str
        available_servers: typing.List[str]

        def set(self):
            """edit project configuration"""
            data = {
                "project[fullname]": self.full_name,
                "project[public]": "1" if self.public else "0",
                "project[description]": self.description,
                "project[software]": "1" if self.jenkins else "0",
                "project[_token]": self.token,
            }
            if self.server is not None:
                data["project[memoryLimitGo]"] = str(self.memory_limit)
                data["project[server]"] = self.server
            self.__project.command("edit", **data)
            self.__description.full_name = self.full_name
            self.__description.public = self.public
            self.__description.description = self.description

    def fetch_config(self) -> Project.Config:
        """fetch project configuration"""
        page = self.get_body("edit")
        form = api.crawler.dict_of_form(page)
        full_name = form["project[fullname]"]
        public = form["project[public]"] == "1"
        description = form["project[description]"]
        self.__description.full_name = full_name
        self.__description.public = public
        self.__description.description = description
        available_servers = []
        select_server = api.crawler.get_child_tag(
            page, "select", attrs={"name": "project[server]"}
        )
        if select_server is not None:
            for server in select_server.find_all("option"):
                available_servers.append(server.text)
        return Project.Config(
            self,
            self.__description,
            full_name=full_name,
            public=public,
            description=description,
            jenkins=form["project[software]"] == "1",
            memory_limit=int(
                form.get("project[memoryLimitGo]", DEFAULT_MEMORY_LIMIT)
            ),
            server=form.get("project[server]", None),
            token=form["project[_token]"],
            available_servers=available_servers,
        )

    def delete(self) -> None:
        """delete project"""
        self.command("delete")

    def join(self) -> None:
        """join project"""
        self.portal.command(
            "project", "join", self.short_name, need_success_message=False
        )

    def suspend(self) -> None:
        """suspend project (CI admin only)"""
        self.portal.command("project", "suspend", self.short_name)

    def resume(self) -> None:
        """resume project (CI admin only)"""
        self.portal.command("project", "resume", self.short_name)

    def download_key(self) -> str:
        """return public SSH key used by project Jenkins instance"""
        response = self.portal.session.get(
            self.portal.url("project", "download", "ssh", self.short_name),
            timeout=self.portal.timeout,
        )
        api.crawler.check_server_response(response)
        return response.text

    def log(self, page: int, limit: int):
        """enumerate project log"""
        logpage = self.get_body("logs", page=page, limit=limit)
        table = api.crawler.get_child_tag(logpage, "table")
        return api.crawler.remap_table(table, api.crawler.PROJECT_LOG_LIST_SPEC)


class Projects(Portal.Section):
    """project management"""

    def __init__(self, portal):
        Portal.Section.__init__(self, portal)
        self.__deleted = DeletedProjects(self.portal)

    # pylint: disable=too-many-arguments
    def create(
        self,
        short_name: str,
        full_name: str,
        public: bool,
        description: str,
        jenkins: bool = True,
    ) -> Project:
        """create a new project"""
        portal = self.portal
        check = self.portal.api_command(
            "project", "check", "shortname", encode_base64(short_name), get=True
        )
        if check["responseCode"] != 200:
            api.crawler.raise_server_error(check["message"])
        page = portal.get("project", "new")
        data = {
            "project[shortname]": short_name,
            "project[fullname]": full_name,
            "project[description]": description,
            "project[public]": "1" if public else "0",
            "project[software]": "1" if jenkins else "0",
        }
        api.crawler.fill_form(page.body, data)
        portal.command("project", "new", **data)
        return Project(
            portal,
            short_name,
            Project.Description(
                full_name=full_name, description=description, public=public
            ),
        )

    def __getitem__(self, short_name: str) -> Project:
        description = Project.Description()
        as_map = {"short_name": short_name}
        return Project(self.portal, short_name, description, as_map=as_map)

    def __list_of_table(self, table) -> typing.Iterable[Project]:
        rows = api.crawler.list_of_table(table)
        if not rows:
            return ()
        if "#" in rows[0]:
            project_dict_list = api.crawler.remap_table_dict(
                rows, api.crawler.PROJECT_LIST_SPEC_ADMIN
            )
            return (
                Project(
                    self.portal,
                    project_dict["short_name"],
                    Project.Description(
                        full_name=project_dict["full_name"],
                        public=project_dict["privacy"] == "Public",
                    ),
                    as_map=project_dict,
                )
                for project_dict in project_dict_list
            )
        project_dict_list = api.crawler.remap_table_dict(
            rows, api.crawler.PROJECT_LIST_SPEC_REGULAR_USER
        )
        return (
            Project(
                self.portal,
                project_dict["short_name"],
                Project.Description(
                    full_name=project_dict["full_name"],
                    description=project_dict["description"],
                    public=True,
                ),
                as_map=project_dict,
            )
            for project_dict in project_dict_list
        )

    def __iter__(self) -> typing.Iterator[Project]:
        """enumerate all projects"""
        projects: typing.List[Project] = []
        page_index = 1
        while True:
            page = self.portal.get("project", page=str(page_index))
            table = list(self.__list_of_table(page.body.find("table")))
            if not table:
                break
            projects.extend(table)
            page_index += 1
        return iter(projects)

    def search(self, query: str) -> typing.Iterable[Project]:
        """enumerate projects matching query"""
        table = self.portal.get("project", page=str(1))
        body = table.body.find("tbody")
        search = self.portal.api_command(
            "projects", "search", encode_base64(query), get=True
        )
        new_body = bs4.BeautifulSoup(search["body"], features="lxml")
        body.clear()
        body.append(new_body)
        return self.__list_of_table(table)

    @property
    def deleted(self) -> DeletedProjects:
        """return deleted projects API"""
        return self.__deleted


class Vm(Project.Section, api.spec.Listable, api.key_indexed.KeyIndexed):
    """virtual machine"""

    class Spec(typing.NamedTuple):
        """virtual machine specification"""

        name: str
        description: str
        template: str
        memory: int
        cpu_number: int
        root_disk_size: int
        additional_disk_size: typing.Optional[int]

    def __init__(self, project: Project, name: str, vm_id: str, as_map=None):
        Project.Section.__init__(self, project)
        api.spec.Listable.__init__(self, as_map)
        self.__name = name
        self.__id = vm_id

    @property
    def name(self) -> str:
        """return virtual machine name"""
        return self.__name

    @property
    def vm_id(self) -> str:
        """return vm ID"""
        return self.__id

    @property
    def status(self) -> str:
        """return VM status"""
        return self.as_map["status"]

    def _key(self):
        return self.__id

    def start(self) -> None:
        """start virtual machine"""
        self.__command("start")

    def stop(self) -> None:
        """stop virtual machine"""
        self.__command("stop")

    def delete(self) -> None:
        """delete virtual machine"""
        self.__command("delete")

    def recover(self) -> None:
        """recover virtual machine"""
        self.__command("recover")

    def __command(self, command: str):
        self.project.api_command(
            "cloudService/CLOUDSTACK/slave", self.vm_id, command
        )


class Vms(Project.Section):
    """project virtual machine management"""

    class Create(Project.Section):
        """project virtual machine creation"""

        def __init__(self, project: Project):
            super().__init__(project)
            self.__project = project
            self.__page = self.project.get("slaves", "new")
            self.__offerings = self.__enumerate_all_offerings()

        def __enumerate_all_offerings(self):
            """enumerate all offerings available for virtual machines of the
            project"""
            page = self.__page

            def enumerate_templates(template_id):
                result = {}
                template_list = page.body.find(
                    "div", {"id": template_id}
                ).find_all("label")
                for template in template_list:
                    key = template.find("input")["value"]
                    result[key] = template.text.strip()
                return result

            def enumerate_options(option_id):
                result = {}
                offers = page.body.find("select", {"id": option_id}).find_all(
                    "option"
                )
                for offer in offers:
                    if offer["value"] != "":
                        result[offer["value"]] = offer.text
                return result

            featured_templates = enumerate_templates("featured")
            community_templates = enumerate_templates("community")
            project_templates = enumerate_templates("self")
            # all_templates merges featured_templates, community_templates and
            # project_templates
            all_templates = dict(featured_templates)  # copy
            all_templates.update(community_templates)
            all_templates.update(project_templates)
            return {
                "featured_templates": featured_templates,
                "community_templates": community_templates,
                "project_templates": project_templates,
                "all_templates": all_templates,
            }

        def __getitem__(self, key):
            return types.MappingProxyType(self.__offerings[key])

        def create(self, spec: Vm.Spec) -> None:
            """create a new virtual machine"""
            data: typing.Dict[str, str] = {
                "newSlave[name]": spec.name,
                "newSlave[description]": spec.description,
                "newSlave[template]": spec.template,
                "newSlave[RAM]": str(spec.memory),
                "newSlave[numberOfCores]": str(spec.cpu_number),
                "newSlave[rootDiskSize]": str(spec.root_disk_size),
            }
            if spec.additional_disk_size is None:
                data["newSlave[additionalDisk]"] = "0"
                data["newSlave[additionalDiskSize]"] = "0"
            else:
                data["newSlave[additionalDisk]"] = "1"
                data["newSlave[additionalDiskSize]"] = str(
                    spec.additional_disk_size
                )
            api.crawler.fill_form(self.__page.body, data)
            self.project.command("slaves", "new", **data)

    def create(self) -> Vms.Create:
        """prepare virtual machine creation"""
        return Vms.Create(self.project)

    def __vm_of_dict(self, machine) -> Vm:
        display_name = machine["display_name"]
        return Vm(self.project, display_name, machine["id"], as_map=machine)

    def __iter__(self) -> typing.Iterator[Vm]:
        """enumerate virtual machines using portal"""
        page = self.project.get("slaves")
        rows = api.crawler.list_of_table(page.body.find("table"))
        if not rows:
            return iter(())
        if "Hostname/IP" in rows[0]:
            spec: api.spec.DictSpec = api.crawler.VM_LIST_SPEC_MEMBER
        else:
            spec = api.crawler.VM_LIST_SPEC_NOT_MEMBER
        vms = api.crawler.remap_table_dict(rows, spec)
        return map(self.__vm_of_dict, vms)

    def __getitem__(self, name) -> Vm:
        vms = {machine.name: machine for machine in self}
        try:
            return vms[name]
        except KeyError:
            return vms[self.project.short_name + "-" + name]


class Member(Project.Section, User):
    """project member"""

    class Description(typing.NamedTuple):
        """member description"""

        uid: str
        admin_id: str

    def __init__(
        self,
        project: Project,
        email: str,
        desc: typing.Optional[Member.Description] = None,
        as_map=None,
    ) -> None:
        Project.Section.__init__(self, project)
        uid: typing.Optional[str] = desc.uid if desc else None
        admin_id: typing.Optional[str] = desc.admin_id if desc else None
        User.__init__(self, project.portal, email, uid, as_map=as_map)
        self.__admin_id = admin_id

    class Role(enum.Enum):
        """member role"""

        ADMIN = "admin"
        SLAVE_ADMIN = "slaveAdmin"

    def set(self, role: Role, value: bool) -> None:
        """set or unset admin or virtual machine admin flag"""
        flag = "set" if value else "unset"
        name = self.project.short_name
        admin_id = self.__admin_id
        self.portal.api_command("updateRole", flag, role.value, name, admin_id)

    def leave(self) -> None:
        """remove member from project"""
        self.project.command(self.email, "leave")


class Members(Project.Section):
    """project member management"""

    def __init__(self, project: Project) -> None:
        super().__init__(project)
        self.__pendings = PendingUsers(project)

    @property
    def pendings(self) -> PendingUsers:
        """return pending user API"""
        return self.__pendings

    def add(self, *users: User) -> None:
        """add members on project"""
        self.project.api_command(
            "users", "add", emails=",".join(user.email for user in users)
        )

    def __iter__(self) -> typing.Iterator[Member]:
        project = self.project
        page = project.get("users")
        table = page.body.find("table", id="active_users_tab")
        rows = api.crawler.list_of_table(table)
        if not rows:
            return iter(())
        if "#" not in rows[0]:
            spec = api.crawler.MEMBER_LIST_SPEC_NOT_MEMBER
            make_desc: typing.Callable[
                [typing.Any], typing.Optional[Member.Description]
            ] = lambda _: None
        else:
            spec = api.crawler.MEMBER_LIST_SPEC_MEMBER
            make_desc = lambda mbd: Member.Description(
                uid=mbd["uid"], admin_id=mbd["_admin_id"]
            )
        member_dict_list = api.crawler.remap_table_dict(rows, spec)
        iterable = (
            Member(project, mbd["email"], desc=make_desc(mbd), as_map=mbd)
            for mbd in member_dict_list
        )
        return iter(iterable)


class PendingUser(Project.Section, User):
    """project pending user"""

    class Desc(typing.NamedTuple):
        """pending user description"""

        email: str
        full_name: str
        handle: str

    def __init__(
        self, project: Project, desc: PendingUser.Desc, as_map=None
    ) -> None:
        Project.Section.__init__(self, project)
        User.__init__(self, project.portal, desc.email, as_map=as_map)
        self.__handle = desc.handle
        self.__full_name = desc.full_name

    @property
    def handle(self):
        """return pending member handle"""
        return self.__handle

    @property
    def full_name(self):
        """return pending user full name mmmm(first name and last name)"""
        return self.__full_name


AnswerUsers = typing.Optional[typing.Iterable[PendingUser]]


class PendingUsers(Project.Section):
    """pending users API"""

    def __pending_user_of_dict(self, pud) -> PendingUser:
        desc = PendingUser.Desc(pud["email"], pud["user"], pud["_handle"])
        return PendingUser(self.project, desc, as_map=pud)

    def __iter__(self) -> typing.Iterator[PendingUser]:
        """enumerate all users pending for joining the project"""
        project = self.project
        form = project.get("users").body.find("form", id=project.short_name)
        pending_dict_list = api.crawler.remap_table(
            form, api.crawler.PENDING_USER_LIST_SPEC
        )
        return iter(map(self.__pending_user_of_dict, pending_dict_list))

    def answer(self, accept: AnswerUsers = None, reject: AnswerUsers = None):
        """accept and/or reject some users"""

        def dict_of_pending_users(users: AnswerUsers, value: str):
            return {pending.handle: value for pending in users} if users else {}

        referer = self.portal.url("project", self.project.short_name, "users")
        self.project.command(
            "pendings",
            headers={"referer": referer},
            **(dict_of_pending_users(accept, "accept")),
            **(dict_of_pending_users(reject, "reject")),
        )


class Jenkins(Project.Section):
    """managing Jenkins instances"""

    def start(self, qualif: bool = False) -> None:
        """start Jenkins instance"""
        self.project.api_command(self.get_instance(qualif), "start")

    def restart(self, qualif: bool = False) -> None:
        """restart Jenkins instance"""
        self.project.api_command(self.get_instance(qualif), "restart")

    def stop(self, qualif: bool = False) -> None:
        """stop Jenkins instance"""
        self.project.api_command(self.get_instance(qualif), "stop")

    def push(self, qualif: bool = False) -> None:
        """replicate continuous integration configuration / jobs
        from production to qualification"""
        self.portal.api_command("push", self.get_instance(qualif))

    def install_prod(self, qualif: bool = False) -> None:
        """push qualification version on production"""
        self.portal.api_command("install", "prod", self.get_instance(qualif))

    def rollback(self, qualif: bool = False) -> None:
        """go back to the previous revision installed on production"""
        self.portal.api_command("rollback", self.get_instance(qualif))

    class Version(Project.Section, api.key_indexed.KeyIndexed):
        """available Jenkins version"""

        def __init__(self, project: Project, full_name: str, code: str):
            super().__init__(project)
            self.__code = code
            try:
                name, lts = full_name.split(" ")
                is_lts = lts == "LTS"
            except ValueError:
                is_lts = False
            self.__name = name if is_lts else full_name
            self.__lts = is_lts

        @property
        def name(self) -> str:
            """return version name (without LTS suffix)"""
            return self.__name

        @property
        def lts(self) -> bool:
            """return whether the version is LTS"""
            return self.__lts

        def _key(self):
            return self.__name

        def install_qualif(self):
            """install the selected version on qualification instance"""
            self.portal.api_command(
                "install", "qualif", self.project.short_name, self.__code
            )

    def __version_of_tag(self, tag: bs4.element.Tag) -> Jenkins.Version:
        return Jenkins.Version(
            self.project,
            tag.text.strip(),
            api.crawler.get_single_attribute(tag, "value"),
        )

    def get_available_versions(self) -> typing.Mapping[str, Jenkins.Version]:
        """enumerate all available Jenkins versions:
        keys are the version names"""
        page = self.project.get("cisoftware")
        versions = map(self.__version_of_tag, page.body.find_all("option"))
        return {version.name: version for version in versions}

    def get_instance(self, qualif: bool) -> str:
        """return instance name"""
        instance_name = self.project.short_name
        if qualif:
            instance_name += "-qualif"
        return instance_name


class DeletedProject(DeletableProject, api.spec.Listable):
    """deleted project"""

    def __init__(self, portal: Portal, short_name: str, as_dict=None) -> None:
        DeletableProject.__init__(self, portal, short_name)
        api.spec.Listable.__init__(self, as_dict)

    def recover(self) -> None:
        """recover a project"""
        self.command("recover")


class DeletedProjects(Portal.Section):
    """deleted project management"""

    def __getitem__(self, short_name: str) -> DeletedProject:
        return DeletedProject(self.portal, short_name)

    def __project_of_dict(self, pdict) -> DeletedProject:
        return DeletedProject(self.portal, pdict["short_name"], as_dict=pdict)

    def __iter__(self) -> typing.Iterator[DeletedProject]:
        project_dict_list = api.crawler.remap_table(
            self.portal.get("admin", "dashboard"),
            api.crawler.DELETED_PROJECT_LIST_SPEC,
        )
        return iter(map(self.__project_of_dict, project_dict_list))


class SubAPIs(typing.NamedTuple):
    """Sub APIs for project"""

    vms: Vms
    members: Members
    jenkins: Jenkins
