"""lazy evaluation decorator

We can find many implementation of this decorator on the Web, for instance:
https://blitiri.com.ar/git/r/pymisc/b/master/t/f=lazy.py.html

However, I didn't find any version which would be well packaged.
"""

import typing

# T seems legit for a type variable!
# pylint: disable=invalid-name
T = typing.TypeVar("T")


class __Lazy(typing.Generic[T]):
    """internal representation of lazy suspension: this is a callable but not
    a function in order to expose computed property."""

    def __init__(self, f: typing.Callable[[], T]):
        # We use a Boolean flag to indicate whether the value has been computed
        # in case of T is nullable.
        self.__f = f
        self.__computed = False
        self.__result: typing.Optional[T] = None

    def __call__(self) -> T:
        if self.__computed:
            # The following cast is justified by the invariant:
            # self.__computed == True => self.__result: T
            return typing.cast(T, self.__result)
        result = self.__f()
        self.__computed = True
        self.__result = result
        return result

    @property
    def computed(self) -> bool:
        """return whether the suspension value has been computed"""
        return self.__computed


def lazy(f: typing.Callable[[], T]) -> typing.Callable[[], T]:
    """given the suspension f (i.e., a function that computes a value without
    any argument), return a lazy suspension, i.e. a suspension that computes
    its result only the first time it is evaluated."""
    return __Lazy(f)
