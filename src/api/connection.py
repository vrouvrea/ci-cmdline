"""basic definitions to handle HTTP connections"""

import abc
import logging
import typing
import urllib.parse

import requests

LOGGER = logging.getLogger(__name__)


class Credentials(typing.NamedTuple):
    """user credentials"""

    username: str
    password: str


def build_url(base: str, *args: str) -> str:
    """build a URL from components"""
    return urllib.parse.urljoin(base, "/".join(args))


class NotConnected(Exception):
    """query while disconnected"""


class QueryError(Exception):
    """exception for query error"""


QueryMethod = typing.Callable[[str], requests.Response]


class CredentialsProvider(abc.ABC):
    """interface for front-end method for getting credentials"""

    @abc.abstractmethod
    def get_credentials(self) -> Credentials:
        """return user credentials"""

    @abc.abstractmethod
    def store_cookies(self, cookies):
        """remember cookies for another session"""
