#!/bin/bash
set -ex
admin_credentials="$SECRETS/thierry_martinez_ci_credentials.yml"
guest_credentials="$SECRETS/guest_ci_credentials.yml"
guest_account="martinez+cicmdline@nsup.org"
export CI_CREDENTIALS="$admin_credentials"

# project list

ci project list
ci --credentials "$guest_credentials" project list
ci project list --user thierry.martinez@inria.fr

# vm list

ci vm list sed-paris
ci --credentials "$guest_credentials" vm list sed-paris display_name cpu

# project set

timestamp="$(date --iso-8601=seconds)"
new_name="Test project for ci-cmdline ($timestamp)"
new_description="This project is used for testing ci-cmdline ($timestamp)"

ci project set test-ci-cmdline --name "$new_name" --public=no --description="$new_description"

set_name=$(ci project list short_name=test-ci-cmdline full_name)
if [ "$set_name" != "$new_name" ]; then
    exit 1
fi

set_privacy=$(ci project list short_name=test-ci-cmdline privacy)
if [ "$set_privacy" != "Private" ]; then
    exit 1
fi

ci project set test-ci-cmdline --public

set_privacy=$(ci project list short_name=test-ci-cmdline privacy)
if [ "$set_privacy" != "Public" ]; then
    exit 1
fi
# test-ci-cmdline should be left public to enable join

# member

if ci member list test-ci-cmdline email="$guest_account"; then
    ci member delete test-ci-cmdline "$guest_account"
    ci member list test-ci-cmdline email="$guest_account" && exit 1
fi
ci --verbose member add test-ci-cmdline "$guest_account"
ci member list test-ci-cmdline email="$guest_account"
ci member delete test-ci-cmdline "$guest_account"
ci member list test-ci-cmdline email="$guest_account" && exit 1
ci --credentials "$guest_credentials" project join test-ci-cmdline
ci member accept test-ci-cmdline "$guest_account"
ci member list test-ci-cmdline email="$guest_account"

# resource-limit

ci resource-limit update test-ci-cmdline --memory 50000 --primary-storage 500
max=$(ci resource-limit list test-ci-cmdline typename=memory max)
if [ "$max" -ne 50000 ]; then
    exit 1
fi
max=$(ci resource-limit list test-ci-cmdline typename=primary_storage max)
if [ "$max" -ne 500 ]; then
    exit 1
fi
ci resource-limit update test-ci-cmdline --memory "*2" --primary-storage "+100"
max=$(ci resource-limit list test-ci-cmdline typename=memory max)
if [ "$max" -ne 100000 ]; then
    exit 1
fi
max=$(ci resource-limit list test-ci-cmdline typename=primary_storage max)
if [ "$max" -ne 600 ]; then
    exit 1
fi
ci resource-limit update test-ci-cmdline --memory 50000 --primary-storage 500
max=$(ci resource-limit list test-ci-cmdline typename=memory max)
if [ "$max" -ne 50000 ]; then
    exit 1
fi
max=$(ci resource-limit list test-ci-cmdline typename=primary_storage max)
if [ "$max" -ne 500 ]; then
    exit 1
fi

# vm create

if ci vm list test-ci-cmdline hostname=test-ci-cmdline-ubuntu; then
    ci vm expunge test-ci-cmdline ubuntu --delete
fi
ci vm create test-ci-cmdline ubuntu ubuntu-20.04 small
ci project download-key test-ci-cmdline
while [ "$(ci vm list test-ci-cmdline hostname=test-ci-cmdline-ubuntu status)" != Running ]; do
    sleep 1
done
ci --verbose member set test-ci-cmdline "$guest_account" --vm-admin=yes
yes | ssh-keygen -b 4096 -N "" -f test_id_rsa
ci --credentials "$guest_credentials" ssh-key add test_id_rsa.pub
uid="$(ci --credentials "$guest_credentials" user uid)"

# add ci-ssh.inria.fr fingerprint
mkdir -p ~/.ssh
echo '|1|rWEPSZ507caf3ieJ4JKNB5iz3q4=|IB+l8NkAYRUNcYCvS/czOaexSow= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA06F0AjjeoJ2zG2l8n8EAX1OXHNeA1ZL8qcOhQKNX1USHv5wgwA8MJwiQIE34kXSNS8gKNLy5Gv+vUEbIl3wSQtHuJwxSriogQ0BdZN7CbyhvyWd/hYXUmk3nmizIYVivnowyywxjmtTUBL2a4xWPuElEKR2WxXz8AQ2Vf7CRuNd0MnT88KaKID6TwJYVq7ggeGlrT5NkfntP73enC2XsZp5wCZxWrTup9mA0191eTkRweKZBTIY4NRZg4YZP9mtdyt0jAAAfTXkGZ3JLIuZG5GQCdsUN5f2wxpUQa4KttWwM5973w5/P/4kC+5Xso+zSNTpQSPVt+sKZrGvzmpUkFQ==' >>~/.ssh/known_hosts

while ! sshpass -pci ssh-copy-id -f -i test-ci-cmdline.pub -oStrictHostKeyChecking=no \
    -oProxyCommand="ssh -i test_id_rsa -W test-ci-cmdline-ubuntu:22 \
        \"$uid\"@ci-ssh.inria.fr" \
    ci@test-ci-cmdline-ubuntu; do
    sleep 1
done
ci --credentials "$guest_credentials" ssh-key delete --all
ci jenkins restart test-ci-cmdline
if [ "$(ci --verbose --retry 30 agent list test-ci-cmdline display_name=test-ci-cmdline-ubuntu offline)" = True ]; then
    ci agent launch test-ci-cmdline test-ci-cmdline-ubuntu
fi
