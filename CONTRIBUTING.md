# How to contribute?

All contributions are welcome!

Please fork and make merge requests to https://gitlab.inria.fr/inria-ci/ci-cmdline/ .
